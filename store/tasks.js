export const state = () => {
  return {
    items: [
      {
        id: 1,
        title: 'Group 1',
        tasks: [
          {
            id: 1,
            title: "Task 1",
            status: 0,
            group_id: 1
          },
          {
            id: 2,
            title: "Task 2",
            status: 0,
            group_id: 1
          },
          {
            id: 3,
            title: "Task 3",
            status: 1,
            group_id: 1
          }
        ]
      },
      {
        id: 2,
        title: 'Group 2',
        tasks: [
          {
            id: 4,
            title: "Task 4",
            status: 1,
            group_id: 2
          },
          {
            id: 5,
            title: "Task 5",
            status: 0,
            group_id: 2
          }
        ]
      },
      {
        id: 3,
        title: 'Group 3',
        tasks: [
          {
            id: 6,
            title: "Task 6",
            status: 1,
            group_id: 3
          },
          {
            id: 7,
            title: "Task 7",
            status: 0,
            group_id: 3
          }
        ]
      },
      {
        id: 4,
        title: 'Group 4',
        tasks: [
          {
            id: 8,
            title: "Task 8",
            status: 1,
            group_id: 4
          },
          {
            id: 9,
            title: "Task 9",
            status: 0,
            group_id: 4
          }
        ]
      },
      {
        id: 5,
        title: 'Group 5',
        tasks: [
          {
            id: 10,
            title: "Task 10",
            status: 1,
            group_id: 5
          },
          {
            id: 11,
            title: "Task 11",
            status: 0,
            group_id: 5
          }
        ]
      },
      {
        id: 6,
        title: 'Group 6',
        tasks: [
          {
            id: 12,
            title: "Task 12",
            status: 1,
            group_id: 6
          },
          {
            id: 13,
            title: "Task 13",
            status: 0,
            group_id: 6
          }
        ]
      }
    ],
  };
};

export const actions = {
  async getItems({commit, state}) {
    const data = state.items
    commit('SET_ITEMS', data);
  },
};

export const mutations = {
  SET_ITEMS(state, items) {
    state.items = items;
  },

  ADD_TASK(state, payload) {
    state.items.filter((gruop, index) => {
      if(gruop.id === payload.group_id){
        state.items[index].tasks.push(payload);
      };
    });
  },

  ADD_GRUOP(state, payload) {
    state.items.unshift({
      id: Date.now(),
      title: payload.title,
      tasks: []
    });
  },

  SET_STATUS(state, payload) {
    state.items.filter((gruop, group_index) => {
      gruop.tasks.filter((task, task_index) => {
        if(task.id === payload.id){
          state.items[group_index].tasks[task_index].status = payload.status;
        };
      });
    });
  },

  SET_GROUP(state, payload) {
    let current_task = null;
    state.items.filter((gruop, group_index) => {
      gruop.tasks.filter((task, task_index) => {
        if(task.id === payload.task_id){
          current_task = state.items[group_index].tasks[task_index];
          state.items[group_index].tasks.splice(task_index, 1)
        };
      });
    });
    if(current_task) {
      state.items.filter((gruop, index) => {
        if(gruop.id === payload.group_id){
          state.items[index].tasks.push(current_task);
        };
      });
    }
  },

  REMOVE_TASK(state, id) {
    state.items.filter((gruop, group_index) => {
      gruop.tasks.filter((task, task_index) => {
        if(task.id === id){
          state.items[group_index].tasks.splice(task_index, 1)
        };
      });
    });
  },

  REMOVE_GRUOP(state, id) {
    state.items.filter((gruop, group_index) => {
      if(gruop.id === id){
        state.items.splice(group_index, 1)
      };
    });
  },

  SORT_TASKS_BY_NAME_ASC(state, id) {
    state.items.filter((gruop, group_index) => {
      if(gruop.id === id){
        state.items[group_index].tasks.sort((a,b) =>
        (b.title.toLowerCase() > a.title.toLowerCase()) ? 1 : ((a.title.toLowerCase() > b.title.toLowerCase()) ? -1 : 0));
      };
    });
  },

  SORT_TASKS_BY_NAME_DESC(state, id) {
    state.items.filter((gruop, group_index) => {
      if(gruop.id === id){
        state.items[group_index].tasks.sort((a,b) =>
        (a.title.toLowerCase() > b.title.toLowerCase()) ? 1 : ((b.title.toLowerCase() > a.title.toLowerCase()) ? -1 : 0));
      };
    });
  },
  SORT_TASKS_BY_STATUS_ASC(state, id) {
    state.items.filter((gruop, group_index) => {
      if(gruop.id === id){
        state.items[group_index].tasks.sort((a,b) => (b.status > a.status) ? 1 : ((a.status > b.status) ? -1 : 0));
      };
    });
  },

  SORT_TASKS_BY_STATUS_DESC(state, id) {
    state.items.filter((gruop, group_index) => {
      if(gruop.id === id){
        state.items[group_index].tasks.sort((a,b) => (a.status > b.status) ? 1 : ((b.status > a.status) ? -1 : 0));
      };
    });
  }

}

export const getters = {
  items: (state) => state.items,
};
