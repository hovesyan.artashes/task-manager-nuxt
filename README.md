build for production and launch server:  
$ npm run build  
$ npm start  


Single Page application build in Nuxt.js.  

using plugins: vue-js-modal  

Create task and group forms has validation for title length.  

To create new group click to "Add Group" button,  
To create new task click to "+" button under task list,  
To remove task/group click to trash can icon,  
To move the task to another group click on it and drag,  
To sort tasks, click on the corresponding button at the top of the list.  
